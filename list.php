<?php
     $sql = 'SELECT * FROM usuario';
     $connection = new mysqli('localhost:3306', 'root', '', 'workshop2');
     $result = $connection->query($sql);
     $users = $result->fetch_all();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>List</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
    <h1>List of Users</h1>
        <table class="table table-light">
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Lastname</th>
            <th>Username</th>
            <th>Actions</th>
        </tr>
        <tbody>
            <?php
            // loop users
                foreach($users as $user) {
                echo "<tr><td>".$user[0]."</td><td>".$user[1]."</td><td>".$user[2]."</td><td>".$user[3]."</td><td><a href=\"Edit.php?id=".$user[0]."\">Edit</a> | <a href=\"eliminar.php?id=".$user[0]."\">Delete</a></td></tr>";
                }
            ?>
        </tbody>
        </table>
        <?php

    $connection->close();
    ?>
    </div>
</body>
</html>